# README #

### What is this repository for? ###

* This is a prometheus exporter for DSTM miner. It's meant to replace most of the "Miner Setup" from [this guide](https://helpfulsu.wordpress.com/2017/12/02/monitor-ewbf-gpu-miner-using-prometheus/).


### How do I get set up? ###

* Follow this [guide](https://helpfulsu.wordpress.com/2017/12/02/monitor-ewbf-gpu-miner-using-prometheus/) which is installing prometheus and grafana.
It has a sample prom config, chooch through that.
* For "Miner Setup", ignore it. Install python2 and configure it in your path. [Miniconda](https://conda.io/miniconda.html) takes care of this for you, it's super slick and I'd recommend it if you don't have python2 installed and setup. This just works.
 * pip install `prometheus_client`
* Run your DSTM miner as usual, if you don't set the telemetry option this angry pixie exporter will work with DSTM's default `http://127.0.0.1:2222` but if you want to use something different make sure to set that here on [line 15]( https://bitbucket.org/threalecurve/angry_pixies/src/440b1e5aa50c6ddf93beb7333142e8eb61761a20/prom_exporter.py?at=master&fileviewer=file-view-default#prom_exporter.py-15)
* Execute this angry pixie exporter
* Now you're at step 4 of the guide and it's the same. Hit the IP of your machine running prometheus at port 9090 to verify stuff is working.
* Continue on to grafana to make the dashboard of your desires.

### Contribution guidelines ###

* make a branch and then a PR out of it, I'm sure I'll see the activity then be able to review and merge it.

### Who do I talk to? ###

* Repo owner
