# requires python2 with prometheus_client
# register python2 in environment vars

import json
import prometheus_client
import sys
import urllib
import urllib2

from prometheus_client import start_http_server, Metric, REGISTRY


def scrape_miner():
    # gets json data from miner rpc
    url = "http://127.0.0.1:2222/jsonrpc"
    data = urllib.urlencode({"id": 1, "method": "getstat"})
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    j = json.loads(response.read())
    return j


class collector(object):
    # posts data to local http for prometheus to scrape

    def ___init___(self):
        pass

    def collect(self):
        payload = scrape_miner()
        # miner metrics
        miner_uptime_seconds = payload.get('uptime')
        miner_conn_time_seconds = payload.get('contime')
        miner_name = payload.get('user').split('.')[1]

        metric = Metric('MINER', 'uptime', 'gauge')
        metric.add_sample('miner_uptime_seconds',
                          value=float(miner_uptime_seconds),
                          labels={'miner_name': miner_name})
        yield metric

        metric = Metric('MINER', 'conn_time', 'gauge')
        metric.add_sample('miner_connection_time_seconds',
                          value=float(miner_conn_time_seconds),
                          labels={'miner_name': miner_name})
        yield metric

        # gpu metrics
        gpus = payload.get('result')
        for gpu in gpus:
            gpu_id = str(gpu.get('gpu_id'))
            gpu_temp = gpu.get('temperature')
            gpu_power = gpu.get('power_usage')
            gpu_speed = gpu.get('sol_ps')
            gpu_efficiency = gpu.get('sol_pw')
            gpu_acceptedshares = gpu.get('accepted_shares')
            gpu_rejectedshares = gpu.get('rejected_shares')

            metric = Metric('GPU', 'temp', 'gauge')
            metric.add_sample('gpu_temp_celcius',
                              value=float(gpu_temp),
                              labels={'gpu': gpu_id,
                                      'miner_name': miner_name})
            yield metric

            metric = Metric('GPU', 'power', 'gauge')
            metric.add_sample('gpu_power_watts',
                              value=float(gpu_power),
                              labels={'gpu': gpu_id,
                                      'miner_name': miner_name})
            yield metric

            metric = Metric('GPU', 'hashrate Sol/s', 'gauge')
            metric.add_sample('gpu_hash_second',
                              value=float(gpu_speed),
                              labels={'gpu': gpu_id,
                                      'miner_name': miner_name})
            yield metric

            metric = Metric('GPU', 'efficiency Sol/W', 'gauge')
            metric.add_sample('gpu_efficiency_sols_per_watt',
                              value=float(gpu_efficiency),
                              labels={'gpu': gpu_id,
                                      'miner_name': miner_name})
            yield metric

            metric = Metric('GPU', 'accepted shares', 'gauge')
            metric.add_sample('gpu_acceptedshares',
                              value=float(gpu_acceptedshares),
                              labels={'gpu': gpu_id,
                                      'miner_name': miner_name})
            yield metric

            metric = Metric('GPU', 'rejected shares', 'gauge')
            metric.add_sample('gpu_rejectedshares',
                              value=float(gpu_rejectedshares),
                              labels={'gpu': gpu_id,
                                      'miner_name': miner_name})
            yield metric


if __name__ == "__main__":
    # start web server and post data
    print ('Starting web server...')
    start_http_server(8000)
    REGISTRY.register(collector())
    print ('Exporting metrics.')
    while True:
        i = raw_input('Type "exit" to exit: ')
        if i.lower() == 'exit':
            sys.exit('Exiting!')
